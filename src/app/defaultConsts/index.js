import { Dimensions } from 'react-native'
export const baseURL = `https://halloffame-server.herokuapp.com`
export const HEIGHT = Dimensions.get("window").height;