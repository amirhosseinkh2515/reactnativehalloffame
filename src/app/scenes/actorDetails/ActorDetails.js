import React, { Component } from 'react'
import { Text, View,StyleSheet,Image } from 'react-native'
import ToggleSwitch from 'toggle-switch-react-native'
import {HEIGHT} from '../../defaultConsts'
import { connect } from 'react-redux'
// import homeImageIcon from './../../../assets/images/home_unselected.png'

class ActorDetails extends Component {
    static navigationOptions = {
        title: "Actor Details",
    };
    constructor(props) {
        super(props)

        this.state = {
            showSpinner: "flex",
            isMarked:false
        }
        console.log("props",this.props)
    }
    render() {
        return (
            <View >
                <View >
                    <View >
                        <Text style={ActorDetailsStyle.name_text}>
                            {this.props.details.name}
                        </Text>
                    </View>
                </View>

                <View >
                    <View style={ActorDetailsStyle.pic_container_inner}>
                        <Image
                            style={ActorDetailsStyle.pic}
                            source={{uri : this.props.details.image}} 
                            />
                    </View>
                    <View >
                        <Text  style={ActorDetailsStyle.name_text}>{this.props.details.dob}</Text>
                    </View>
                    <View style={ActorDetailsStyle.mark_fame_component}>
                        <Text  style={ActorDetailsStyle.mark_text}> Mark This Fame ? </Text>
                        <ToggleSwitch  isOn={this.state.isMarked} onToggle={()=>this.setState({isMarked : !this.state.isMarked})}/>
                    </View>
                </View>
            </View>
        )
    }
}
const mapStateToProps = state => ({ details: state.getDetails.userDetails })
export default connect(mapStateToProps, null)(ActorDetails)


const ActorDetailsStyle = StyleSheet.create({
    main: {
        flexWrap: "wrap",
        flexDirection: "row",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        width: "100%",
        height: HEIGHT*.15,
        marginBottom: "2%",
        borderWidth: 1,
        borderColor: "#0f758c",
        borderRadius: 5,
    },

     pic_container_inner:{
        width: "100%",
        height: "85%",
    },
    pic:{
        width: "100%",
        height: "100%",
        borderRadius: 3,
    },
     name_text:{
        fontSize:12,
        textAlign:"center",
        margin: 0,
        color:"#111",
    },
    mark_text:{
        fontSize:12,
        textAlign:"center",
        margin: 0,
        color:"#111",
        marginRight:"20%"
    },
    dob_container:{
        flexWrap: "wrap",
        flexDirection: "column",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        width: "40%",
        height: "100%",
        padding: "1%",
    },
    mark_fame_component:{
        flexWrap: "wrap",
        flexDirection: "row",
        // backgroundColor:"#c4c4c4",
        margin:"5%"
    },

})
