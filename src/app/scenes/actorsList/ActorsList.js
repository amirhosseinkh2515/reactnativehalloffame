import React, { useState, useEffect, Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, ScrollView, FlatList, Alert } from 'react-native'
import { Spinner } from 'native-base'
import { baseURL } from '../../defaultConsts'
import { connect } from 'react-redux'
import FameList from '../../components/fameList/FameList'
import { userAction } from '../../../redux/user/UserAction'
import { actorDetailsAction } from '../../../redux/actorDetails/ActorDetailsAction'


class ActorsList extends Component {
    static navigationOptions = {
        title: "Choose Your Fame",
        headerLeft: null
    };
    constructor() {
        super()

        this.state = {
            data: [],
            curTime: "00:00",
            showSpinner: "flex"
        }

    }
    componentDidMount() {
        this.fetchData()
        setInterval(() => {
            this.setState({
                curTime: new Date().toLocaleString()
            })
        }, 1000)
    }

    async fetchData() {
        const result = await fetch(`${baseURL}/fames?guest=true`, {
            method: "GET",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
        });
        let firstResponse = await result;
        console.log("firstResponse", firstResponse)
        if (firstResponse.status == 200) {
            const json = await firstResponse.json()
            this.setState({
                data: json.data.list,
                showSpinner: "none"
            })
        }
        else {
            alert("error")
        }
    };
    renderItem = ({ item, index }) => {
        {
            return <TouchableOpacity onPress={() => this._onPress(item)}>
                <FameList index={index} item={item} />
            </TouchableOpacity>
        }
    }
    keyExtractor = (item) => item.id
    _onPress(item) {
        this.props.navigation.navigate('ActorDetails', {
            item
        });
        this.props.setItem(item)
    }
    LogOut() {
        Alert.alert("Caution", `Are You Sure You Want to Log Out ?`, [
            {
                text: 'yes', onPress: () => {
                    this.props.setEmail("")
                    this.props.navigation.navigate("Login")
                }
            },
            { text: 'no', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ], { cancelable: true });
    };
    render() {
        return (
            <View style={ActorsListStyle.main}>
                <Text>Time is : {this.state.curTime.substring(9, 14)}</Text>
                <View style={ActorsListStyle.list_container}>
                    <Spinner style={[ActorsListStyle.spinner, { display: `${this.state.showSpinner}` }]} />
                    <FlatList
                        data={this.state.data}
                        renderItem={({ item, index }) => this.renderItem({ item, index })}
                        keyExtractor={this.keyExtractor}
                    />
                </View>
                <TouchableOpacity onPress={() => this.LogOut()}
                // className="ButtLogginCon" 
                >
                    <Text style={ActorsListStyle.Log_out_text}>{"Log Out"}</Text>
                </TouchableOpacity>
            </View >
        )
    }


}
const mapDispatchToProps = dispatch => {
    return {
        setEmail: mail => {
            dispatch(userAction(mail))
        },
        setItem: item => {
            dispatch(actorDetailsAction(item))
        },
    }
}
export default connect(null, mapDispatchToProps)(ActorsList)


const ActorsListStyle = StyleSheet.create({
    main: {
        flexWrap: "wrap",
        flexDirection: "column",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
    },
    list_container: {
        width: "90%",
        height: "100%",
        marginTop: 20,
        flex: 1,
        flexWrap: "wrap",
        marginTop: "10%",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    spinner: {
        position: "absolute",
        left: "45%"
    },
    Log_out_text:{
        backgroundColor:"pink",
    }
})