import React, { Component } from 'react'
import { Text, View, ImageBackground, StyleSheet, 
    TouchableOpacity, ScrollView, Alert,Dimensions } from 'react-native'
import { Item, Form, Input, Spinner } from 'native-base';
import { userAction } from '../../../redux/user/UserAction'
import { connect } from 'react-redux'
const HEIGHT = Dimensions.get("window").height;

export class Login extends Component {
    static navigationOptions = {
        title: "Back",
        header: null,
    };
    constructor() {
        super()

        this.state = {
            userName: "",
            password: "",
            spinner: "none",
            showText: "flex",
        }
    }
    enter() {
        const { userName, password } = this.state
        if (userName.length >= 10 && password !== "") {
            Alert.alert("You Have Successfully Registered")
            this.props.setEmail(this.state.userName)
            this.props.navigation.navigate("ActorsList")
        }
        else if (userName.length >= 10 && password == "") {
            Alert.alert("Password can not be empty")
        }
        else {
            Alert.alert("Please enter a valid email")
        }
    }
    render() {
        return (
            <ScrollView>
                <View style={LoginStyle.main}>
                    <View style={LoginStyle.inner_container}>
                        <Form>
                            <Item style={LoginStyle.input_container}>
                                <Input
                                    style={LoginStyle.input_text}
                                    onChangeText={(e)=>this.setState({userName:e})}
                                    placeholder="Please Enter Your Email"
                                    placeholderTextColor="rgba(1,1,1,0.5)"
                                    keyboardType="numeric"
                                />
                            </Item>
                            <Item style={LoginStyle.input_container}>
                                <Input
                                    style={LoginStyle.input_text}
                                    onChangeText={(e)=>this.setState({password:e})}
                                    placeholder="Please Enter Your Password"
                                    placeholderTextColor="rgba(1,1,1,0.5)"
                                    keyboardType="numeric"
                                />
                            </Item>
                        </Form>
                    </View>
                    <View style={LoginStyle.button_container}>
                        <TouchableOpacity style={LoginStyle.button}
                            onPress={this.enter.bind(this)}
                        >
                            <Text style={[LoginStyle.button_text, { display: `${this.state.showText}` }]}> {"Submit"} </Text>
                            <Spinner style={{ display: `${this.state.spinner}`, paddingBottom: 5 }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setEmail: email => {
            dispatch(userAction(email))
        }
    }
}
export default connect(null, mapDispatchToProps)(Login)

const LoginStyle = StyleSheet.create({
    main: {
        flex: 1,
        flexWrap: "wrap",
        flexDirection: "row",
        justifyContent: "center",
        alignContent: "flex-end",
        alignItems: "center",
        backgroundColor: "#e2f8ff",
    },
    inner_container: {
        width: "100%",
        height: HEIGHT * 0.7,
        justifyContent: "center",
    },
    splash_screen_version_text: {
        fontSize: 20
    },
    input_container: {
        marginLeft: "15%",
        marginTop: "2%",
        width: "70%",
        justifyContent: "center",
        borderBottomColor: "#fff",
        borderBottomWidth: 3,
    },
    input_text: {
        width: "100%",
        alignItems: "center",
        alignSelf: 'center',
        textAlign: "center",
        justifyContent: "center",
        color: "#fff",
        backgroundColor: "rgba(50,122,122,0.5)",
    },
    button_container: {
        width: "100%",
        height: HEIGHT * 0.30,
        justifyContent: "flex-start",
        alignItems: "center",
    },
    button: {
        width: "70%",
        height: "20%",
        backgroundColor: "#fff",
        justifyContent: "center",
        marginTop: "5%",
        alignItems: "center",
        borderRadius: 5,
    },
    button_text: {
        justifyContent: "center",
        color: "#E28913",
        fontSize: 20,
        paddingBottom: "2%"
    },
})