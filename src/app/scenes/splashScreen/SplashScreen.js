import React, { Component } from 'react'
import { StyleSheet, ImageBackground, Dimensions, Platform } from 'react-native';
import splashScreen from './../../../assets/images/splash-screen.png'
import { Text, View } from 'react-native'
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux'

class SplashScreen extends Component {
    componentDidMount(){
        setTimeout(()=>{
            if(this.props.emailRecived){
               this.props.navigation.navigate("ActorsList")
            }
            else{
                this.props.navigation.navigate("Login")
            }
        },2000)
    }

    render(){
        let readableVersion = DeviceInfo.getReadableVersion();
        return (
            <View style={SplashScreenStyle.main} >
                <ImageBackground source={splashScreen} style={SplashScreenStyle.splash_screen_background_image}>
                </ImageBackground>
                <Text style={SplashScreenStyle.splash_screen_version_text}> Version : {readableVersion} </Text>
            </View>
        )
    }
   
}
const mapStateToProps = state => ({ emailRecived: state.users.email })
export default connect(mapStateToProps, null)(SplashScreen)



const SplashScreenStyle = StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
        flexWrap: "wrap",
        backgroundColor: "#D3D4D6",
    },
    splash_screen_background_image: {
        width: "100%",
        flex: 1,
        resizeMode: 'cover'
    },
    splash_screen_version_text: {
        fontSize: 20
    }
})