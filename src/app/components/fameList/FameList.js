import React, { useState, useEffect, Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native'
import { actorDetailsAction } from '../../../redux/actorDetails/ActorDetailsAction'
import { connect } from 'react-redux'
import {HEIGHT} from '../../defaultConsts'

const FameList = (props) => {
    const item = props.item

    return (
        <View
            style={FameListStyle.main}>
            <View style={FameListStyle.pic_container}>
                <View style={FameListStyle.pic_container_inner}>
                    <Image style={FameListStyle.pic} source={{ uri: item.image }} />
                </View>
            </View>
            <View style={FameListStyle.name_container}>
                <Text style={FameListStyle.name_text}>{item.name}</Text>
            </View>
            <View style={FameListStyle.dob_container}>
                <Text style={FameListStyle.name_text}>{item.dob}</Text>
            </View>
        </View>
    )
}
export default FameList

const FameListStyle = StyleSheet.create({
    main: {
        flexWrap: "wrap",
        flexDirection: "row",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        width: "100%",
        height: HEIGHT*.15,
        marginBottom: "2%",
        borderWidth: 1,
        borderColor: "#0f758c",
        borderRadius: 5,
    },
    pic_container:{
        flexWrap: "wrap",
        flexDirection: "column",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        width: "18%",
        height: "100%",
    },
    pic_container_inner:{
        width: "100%",
        height: "85%",
    },
    pic:{
        width: "100%",
        height: "100%",
        borderRadius: 3,
    },
    name_container:{
        flexWrap: "wrap",
        flexDirection: "column",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        width: "40%",
        height: "100%",
        padding: "1%",
    },
    name_container:{
        flexWrap: "wrap",
        flexDirection: "column",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        width: "40%",
        height: "100%",
        padding: "1%",
    },
    name_text:{
        fontSize:12,
        textAlign:"center",
        margin: 0,
        color:"#111",
    },
    dob_container:{
        flexWrap: "wrap",
        flexDirection: "column",
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        width: "40%",
        height: "100%",
        padding: "1%",
    },

})