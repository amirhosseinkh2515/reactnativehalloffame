
import React from 'react';
import SplashScreen from './app/scenes/splashScreen/SplashScreen'
import Login from './app/scenes/login/Login'
import ActorsList from './app/scenes/actorsList/ActorsList'
import ActorDetails from './app/scenes/actorDetails/ActorDetails'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Provider, connect } from 'react-redux';
import store from './configs/redux/Store'

const App: () => React$Node = () => {
  const RouterWithRedux = connect()(Main)
  return (
    <Provider store={store}>
      <RouterWithRedux />
    </Provider>
  )
}

const container = createStackNavigator({
  SplashScreen,
  Login,
  ActorsList,
  ActorDetails
});

const Main = createAppContainer(container);

export default App;
