import reducers from './Reducers'
import { createStore , compose , applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, reducers)


const middleWare = [thunk]
middleWare.push(createLogger())


 export default store = createStore(
        persistedReducer,
        undefined,
        compose(
            applyMiddleware(...middleWare)
        )
    )


  export const persistor = persistStore(store);