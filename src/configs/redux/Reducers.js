import { combineReducers } from 'redux';
import users from '../../redux/user/UserReducer'
import getDetails from '../../redux/actorDetails/ActorDetailsReducers'




const reducers = combineReducers({
    users,
    getDetails
})

export default reducers