import {SET_FAME_DETAILS} from './ActorDetailsConstants'

const initialState = {
    userDetails:"",
}

const getDetails = (state = initialState , action={})=>{
    switch (action.type){
        case SET_FAME_DETAILS :
            const {payload} = action
            return {
                userDetails :payload
            }
            break;
        default : return state
    }
}

export default getDetails