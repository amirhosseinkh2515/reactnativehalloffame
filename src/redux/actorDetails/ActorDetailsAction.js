import {SET_FAME_DETAILS} from './ActorDetailsConstants'
  
  export function actorDetailsAction (payload) {
    return {
      type: SET_FAME_DETAILS,
      payload
    }
  }