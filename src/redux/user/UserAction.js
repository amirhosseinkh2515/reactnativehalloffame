import {SET_USERS} from './UserConstants'
  
  export function userAction (payload) {
    return {
      type: SET_USERS,
      payload
    }
  }