import {SET_USERS} from './UserConstants'

const initialState = [{
    email :""
}]

const users = (state = initialState , action={})=>{
    switch (action.type){
        case SET_USERS :
            const {payload} = action
            return {
                email:payload
            }
            break;
        default : return state
    }
}

export default users